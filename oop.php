<?php

/**
 * Hewan
 */
trait Hewan
{
    public function __construct($nama, $darah = 50, $jumlahKaki, $keahlian)
    {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }

    public function printKeahlian()
    {
        echo $this->nama . " sedang " . $this->keahlian . ".\n";
    }
}

/**
 * Fight
 */
trait Fight
{
    public function __construct($attackPower, $defencePower)
    {
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function serang($lawan)
    {
        echo $this->nama . " sedang menyerang " . $lawan->nama . "\n\n";
        $lawan->diserang($this);
    }

    public function diserang($lawan)
    {
        if ($this->darah > 0) {
            $this->darah = $this->darah - ($lawan->attackPower / $this->defencePower);
            echo $this->nama . " sedang diserang.\n\n";
        } else {
            echo $this->nama . " sudah meninggal.\n\n";
        }
    }
}

/**
 * Identity
 */
trait Identity
{
    use Hewan, Fight;
    public function __construct($nama, $darah = 50, $jumlahKaki, $keahlian, $attackPower, $defencePower)
    {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        echo "Nama: " . $this->nama . "\nDarah: " . $this->darah . "\nJumlah Kaki: " . $this->jumlahKaki . "\nKeahlian: " . $this->keahlian . "\nAttack Power: " . $this->attackPower . "\nDefence Power: " . $this->defencePower . "\n\n";
    }
}

class Harimau
{
    use Identity;
}

class Elang
{
    use Identity;
}


$harimau = new Harimau("Hamimaw", 50, 4, "menerkam", 50, 5);
$elang = new Elang("Elang", 50, 4, "mencakar", 43, 5);

$harimau->getInfoHewan();
$elang->getInfoHewan();

$harimau->serang($elang);
$harimau->diserang($elang);

$harimau->getInfoHewan();
$elang->getInfoHewan();
